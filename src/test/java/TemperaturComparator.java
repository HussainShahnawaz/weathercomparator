import ApiWeather.WeatherReportByApi;
import UiWeather.CallingUiWeatherReport;
import Utility.BrowserSetup;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class TemperaturComparator {

    public static WebDriver driver;


        @BeforeMethod
        //Setting the browser
        public void BrowserSetup() throws IOException {
            BrowserSetup BS = new BrowserSetup();
            driver = BS.Browsersetup();
        }

        @Test
        public void RunTest() throws ParseException, IOException {

            //Calling Api Weather Class And Creating its object and calling method
            WeatherReportByApi WeatherByApi = new WeatherReportByApi();
            int TempApi = WeatherByApi.wetherapitest();

            //Calling Ui Weather Class And Creating its object and calling methods of it
            CallingUiWeatherReport GettingWeaterReport = new CallingUiWeatherReport();
            int TempUi = GettingWeaterReport.gettingData(driver);

            //Finding the difference between the temp from Api And ui testing
            int Difference = TempApi-TempUi;
            System.out.println("Api Temperature in Celsius is:" +TempApi);
            System.out.println("UI Temperature in Celsius is :" +TempUi);
            System.out.println("Difference is:" +Difference);


            //Comparing and printing the paased or failed test
            if(Difference<=3 && Difference>-3){
                System.out.println("Test Passed");
            }
            else {
                System.out.println("Test Failed");
            }
        }
    @AfterMethod
    public void quit(){
            driver.quit();
    }


}
