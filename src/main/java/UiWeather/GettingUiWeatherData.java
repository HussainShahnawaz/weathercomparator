package UiWeather;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GettingUiWeatherData extends pageInit {


    public WebDriver driver;

    public GettingUiWeatherData(WebDriver driver){
        super(driver);
        this.driver=driver;
    }


    @FindBy(name = "query")
    WebElement SearchInput;

    @FindBy(linkText = "/web-api/three-day-redirect?key=188647&target=")
    WebElement CityClick;

    @FindBy(xpath = "/html/body/div/div[5]/div[1]/div[1]/a[1]")
    WebElement MoreDetails;

    @FindBy(xpath = "//div[@id='dismiss-button']/div")
    WebElement Ads;

    @FindBy(xpath = "//div[@class='temp']")
    WebElement TempDisplay;

    @FindBy(xpath = "//div[@class='current-weather-details']/div[1]/div[3]/div[2]")
    WebElement Humidity;


    public void inputCityDetails(String Place){
        SearchInput.sendKeys(Place+ Keys.RETURN);

    }

    public void detailedreport(){
        MoreDetails.click();
    }

    public void adsHandling(){
        driver.switchTo().frame("google_ads_iframe_/6581/web/in/interstitial/weather/local_home_0");
        Ads.click();
    }

    public String displayingTheTemperature(){
       return TempDisplay.getText();

    }

    public String displayingHumidity(){
        return Humidity.getText();
    }

}
