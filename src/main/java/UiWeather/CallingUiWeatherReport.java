package UiWeather;

import Utility.ReadProperties;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;

public class CallingUiWeatherReport {
    Properties prop;



    public int gettingData(WebDriver driver) throws IOException {

        //Creating object of ReadProperties class and puting it in the Properties class
        ReadProperties rp = new ReadProperties();
        prop = rp.getproperty();
        GettingUiWeatherData Wui = new GettingUiWeatherData(driver);

        //Calling Place details from Property file
        Wui.inputCityDetails(prop.getProperty("Place"));
        Wui.detailedreport();
        Wui.adsHandling();
        String tempUI =   Wui.displayingTheTemperature();

        //Converting the Data into integer and removing all the other Strings
        int TempetrtureUI = Integer.parseInt(tempUI.replaceAll("[^\\d]",""));
        return TempetrtureUI;

    }
}
