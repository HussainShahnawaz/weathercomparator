package ApiWeather;

import Utility.ReadProperties;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class WeatherReportByApi {
    Properties prop;
    public int wetherapitest() throws ParseException, IOException {

        ReadProperties rp = new ReadProperties();
        prop = rp.getproperty();

        Response response=given()
                .queryParam("q", prop.getProperty("Place"))
                .queryParam("appid",prop.getProperty("ApiKey"))
                .when()
                .get(prop.getProperty("BasePath"));

        JSONParser p= new JSONParser();
        Object obj=p.parse(response.asString());
        JSONObject jObj =  (JSONObject)obj;
        JSONObject MainObj= (JSONObject) jObj.get("main");
        Double Temp = (Double) MainObj.get("temp");
        int TempinCelcious = (int) (Temp - 273.15);
        return TempinCelcious;
    }
}
