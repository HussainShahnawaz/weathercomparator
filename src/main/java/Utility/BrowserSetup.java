package Utility;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BrowserSetup {

    WebDriver driver;
    public WebDriver Browsersetup() throws IOException {

        ReadProperties rp = new ReadProperties();
        Properties prop = rp.getproperty();

        if(prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            ChromeOptions opt = new ChromeOptions();
            opt.addArguments("--disable-notifications");
             driver = new ChromeDriver(opt);
             driver.get(prop.getProperty("url"));
             driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return driver;
        }
        else {
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions opt = new FirefoxOptions();
            opt.addPreference("dom.webnotifications.enable",false);
            driver = new FirefoxDriver(opt);
            driver.get(prop.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return driver;
        }
    }
}
